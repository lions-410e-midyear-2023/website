## DISABLE

Districts 410W and 410E District Governor Elect training
20/04/30
14:00-17:30
[Riverside Hotel](/venue)

Training will be provided for the District Governors Elect of Districts 410W and 410E. Contact your District or MD GLT Coordinator for more information.
