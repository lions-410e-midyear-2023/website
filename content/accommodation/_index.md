---
title: "Lions District 410E 2023 Midyear Conference Accommodation"
draft: false
---

Accommodation bookings must be made directly with the [Stonehenge Lodge in Parys](https://www.stoneza.co.za/). Contact Linda on 082 062 3155 or [reservations1@stoneza.co.za](mailto:reservations1@stoneza.co.za.). Stonehenge has 61 rooms. The rates include breakfast:

- R870 per person per night in a single room
- R570 per person per night in a sharing room

Overflow guests will be accommodated at Egweni River Lodge, located 5.6 km from Stonehenge. A shuttle service will be provided.
