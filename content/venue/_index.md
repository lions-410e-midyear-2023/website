---
title: "Lions MD410 2022 Convention Venue"
draft: false
---

The Lions MD410 2022 Convention will be held at the [Buffalo Club](https://buffsclub.com/) in East London.

The venue does not have attached accommodation, but there are a number of B&B's and accommodation options nearby. A list of accommodation options can be found [here](https://www.afristay.com/a/east-london/bed-and-breakfast/).

Delegates are free to make whatever arrangements suit them best - there is no accommodation component in the registration fee.
