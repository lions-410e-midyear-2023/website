---
title: "Lions District 410E 2023 Midyear Conference"
date: 2019-07-14T16:19:07+02:00
draft: false
---

Welcome to the website of the 2023 [Lions District 410E](https://www.lions410e.org.za) Conference to be held 3 to 5 November at the Stonehenge River Lodge, Parys.

Registeration for the conference has closed. Please contact the convenors with any questions.

<!-- Click [here](/news/newsflash_01) to view the latest convention newsletter. -->
