---
title: "Newsflash 1"
date: 2019-07-14T16:19:07+02:00
draft: false
---

<div class="text-center">
    <img src="/docs/newsflash_01.png" width="600" alt="Newsflash 01" class="rounded img-fluid">
</div>

[Download as a PDF](/docs/newsflash_01.pdf)
