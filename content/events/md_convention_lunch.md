---
title: 'Convention delegates lunch'
draft: false
---

Lunch for delegates to the 2021 MD410 Convention.
\
\
**Date and Time**: Saturday 01 May 2021, 12:45-13:30 \
**Location**: [Riverside Hotel](/venue)
\
\
[Back to Program](/program)
