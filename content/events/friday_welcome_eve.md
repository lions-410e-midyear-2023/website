---
title: 'Welcome Evening'
draft: false
---

The welcome evening, including dinner.
\
\
**Date and Time**: Friday 03 November 2023, 18:00- \
**Location**: Main Lodge
\
\
[Back to Program](/program)
