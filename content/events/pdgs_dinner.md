---
title: 'PDGs dinner'
draft: false
---

The traditional dinner for all attending PDGs and the current DGs. **This event is not covered by the Convention registration and is an additional cost for the attendees.**

Please indicate on your registration form if you will be attending the PDGs dinner, or contact the Registration Secretary Kim van Wyk if you have already registered for the Convention and wish to attend the PDGs dinner.
\
\
**Date and Time**: Thursday 29 April 2021, 19:00- \
**Location**: [Riverside Hotel](/venue)
\
\
[Back to Program](/program)
