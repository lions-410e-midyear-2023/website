---
title: 'Regional Lions Leadership Institute Dinner'
draft: false
---

Dinner for the attendees of the RLLI. See [here](/rlli) for more details.
\
\
**Date and Time**: Wednesday 28 April 2021, 18:30- \
**Location**: [Riverside Hotel](/venue)
\
\
[Back to Program](/program)
