---
title: 'Breakfast at the hotel'
draft: false
---

More details will be made available closer to the event.
\
\
**Date and Time**: Saturday 01 May 2021, 07:00-08:00 \
**Location**: [Riverside Hotel](/venue)
\
\
[Back to Program](/program)
