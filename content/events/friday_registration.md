---
title: 'Registration Desk Open'
draft: false
---

The registration desk will be open in the lodge reception.
\
\
**Date and Time**: Friday 03 November 2023, 12:00-17:00 \
**Location**: Reception
\
\
[Back to Program](/program)
