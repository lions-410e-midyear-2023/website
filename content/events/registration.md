---
title: 'Registration'
draft: false
---

The registration desk will also be manned during the remainder of the day, where possible.
\
\
**Date and Time**: Friday 30 April 2021, 08:00-10:30 \
**Location**: [Riverside Hotel](/venue)
\
\
[Back to Program](/program)
