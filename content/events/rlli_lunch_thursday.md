---
title: 'Regional Lions Leadership Institute Lunch'
draft: false
---

Lunch for the attendees of the RLLI. See [here](/rlli) for more details.
\
\
**Date and Time**: Wednesday 28 April 2021, 12:30-13:30 \
**Location**: [Riverside Hotel](/venue)
\
\
[Back to Program](/program)
