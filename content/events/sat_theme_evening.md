---
title: 'Theme Evening'
draft: false
---

The theme evening, including dinner.
\
\
**Date and Time**: Saturday 04 November 2023, 18:00- \
**Location**: Main Lodge
\
\
[Back to Program](/program)
