---
title: 'Presidents Elects Breakfast'
draft: false
---

Breakfast and discussion for incoming club presidents with the Council Chair.
\
\
**Date and Time**: Saturday 01 May 2021, 07:00-08:30 \
**Location**: [Riverside Hotel](/venue)
\
\
[Back to Program](/program)
