---
title: 'Registration Desk Open'
draft: false
---

The registration desk will be open in the lodge reception.
\
\
**Date and Time**: Saturday 04 November 2023, 07:00-09:00 \
**Location**: Reception
\
\
[Back to Program](/program)
