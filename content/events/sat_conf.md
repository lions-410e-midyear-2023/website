---
title: 'Midyear Conference'
draft: false
---

The Midyear Conference.
\
\
**Date and Time**: Saturday 04 November 2023, 09:00-13:00 \
**Location**: Function Room
\
\
[Back to Program](/program)
