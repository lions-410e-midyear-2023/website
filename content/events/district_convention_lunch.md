---
title: 'Convention delegates lunch'
draft: false
---

Lunch for delegates to the 2021 MD410W and MD410E District Conventions.
\
\
**Date and Time**: Friday 30 April 2021, 12:45-13:30 \
**Location**: [Riverside Hotel](/venue)
\
\
[Back to Program](/program)
