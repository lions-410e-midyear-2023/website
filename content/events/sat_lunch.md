---
title: 'Lunch'
draft: false
---

Lunch will be setup at the river.
\
\
**Date and Time**: Saturday 04 November 2023, 13:00- \
**Location**: River
\
\
[Back to Program](/program)
