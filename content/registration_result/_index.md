---
title: Registration Form Submission
draft: false
---

Thank you for submitting your registration form for the 2023 Lions District 410E Midyear Conference. 

A copy of your registration form will be emailed to you. If you have not received anything within 3 days, please contact Kim van Wyk on [vanwykk@gmail.com](vanwykk@gmail.com) or 083 384 4260 (please use WhatsApp rather than a phone call if possible).

For queries on the conference, please contact the convening committee on [midyear@lions410e.org.za](midyear@lions410e.org.za).

Thank you again for registering!

<a href="/registration">Click here to register another attendee</a>

<center><a href="/">Back to the home page</a></center>
