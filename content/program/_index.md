
---
title: "Lions District 410E 2023 Midyear Conference Program"
draft: false
---

The Lions District 410E 2023 Midyear Conference will be held on Friday 3 November and Saturday 4 November 2023 at the Stonehenge Lodge in Parys.

## Friday 03 November 2023

Time | Event (click on event for further details) | Venue
 ---|---  |---
12:00-17:00 | [Registration Desk Open](/events/friday_registration) | Reception
14:00- | [Accommodation Checkin Open](/events/friday_checkin) | Reception
18:00- | [Welcome Evening](/events/friday_welcome_eve) | Main Lodge

## Saturday 04 November 2023

Time | Event (click on event for further details) | Venue
 ---|---  |---
06:30-09:00 | [Breakfast](/events/sat_breakfast) | Main Lodge
07:00-09:00 | [Registration Desk Open](/events/sat_registration) | Reception
09:00-13:00 | [Midyear Conference](/events/sat_conf) | Function Room
13:00- | [Lunch](/events/sat_lunch) | River
18:00- | [Theme Evening](/events/sat_theme_evening) | Main Lodge

## Sunday 05 November 2023

Time | Event (click on event for further details) | Venue
 ---|---  |---
07:00-10:00 | [Breakfast](/events/sun_breakfast) | Main Lodge
